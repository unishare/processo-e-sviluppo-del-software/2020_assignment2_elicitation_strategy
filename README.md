# Assignment 2

## Repository link

- [https://gitlab.com/unishare/processo-e-sviluppo-del-software/2020_assignment2_elicitation_strategy](https://gitlab.com/unishare/processo-e-sviluppo-del-software/2020_assignment2_elicitation_strategy)

## Autori

- 829470 Federica Di Lauro
- 829827 Davide Cozzi 
- 829835 Gabriele De Rosa

### Documento
Si può trovare il documento pdf negli artifact delle [pipeline](https://gitlab.com/unishare/processo-e-sviluppo-del-software/2020_assignment2_elicitation_strategy/-/pipelines).

### Dati dei questionari

Nella cartella `assets/data` si trovano i risultati del questionario in formato
`csv`.
